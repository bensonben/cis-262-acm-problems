//
//  main.cpp
//  2019
//
//  Created by Benjamin J. Benson on 11/30/15.
//  Copyright (c) 2015 Benjamin J. Benson. All rights reserved.
//

#include <iostream>
#include <list>
#include <istream>
#include <deque>
#include <string>
#include <sstream>
#include <algorithm>
using namespace std;

void read_data(istream &data);
void move_car(char c);
void create_list(string &io);

list <char> parked_cars;
deque <char> unpark_cars;

void read_data(istream &data){
    
    string original_order;
    int num_moves;
    char c;
    data >> original_order >> num_moves;
    
    create_list(original_order);
    
    int dataset = 1;
    cout << "\nDataset #" <<dataset<<": The initial order is "<<original_order<<"."<<endl;
    dataset++;
    
    while(num_moves > 0){
        num_moves--;
        data>>c;
        
        move_car(c);
        
        
        cout << "After "<<c<<" leaves, the order is ";
        
        for(std::list<char>::iterator i = parked_cars.begin(); i != parked_cars.end(); ++i){
            cout << *i;
        }
        cout << "."<<endl;
        //
        //        cout <<endl;
        if(num_moves == 0){
            data >> original_order;
            parked_cars.clear();
            create_list(original_order);
            //            cout << "The original order: "<<original_order<<endl;
            if(original_order == "STOP"){
                break;
            }else{
                data >> num_moves;
                cout << "\nDataset #" <<dataset<<": The initial order is "<<original_order<<"."<<endl;
                dataset++;
                if(num_moves == 0){
                    while(num_moves == 0){
                        data >> original_order;
                        if(original_order == "STOP"){
                            break;
                        }
                        data >> num_moves;
                        cout << "\nDataset #" <<dataset<<": The initial order is "<<original_order<<".";
                        dataset++;
                    }
                }
                //                cout << "The number of moves: "<<to_string(num_moves)<<endl;
            }
        }
        
    }
}

void create_list(string &io){
    
    for(std::string::iterator i = io.begin(); i != io.end(); ++i){
        parked_cars.push_back(*i);
    }
}

void move_car(char c){
    int j = 0;
    
    std::list<char>::iterator place_holder = parked_cars.end();
    
    for (std::list<char>::iterator i = parked_cars.begin(); i != parked_cars.end(); ++i) {
        
        if(*i == c){
            
            place_holder = i;
            
            if(j <= parked_cars.size()/2){
                std::list<char>::iterator k = parked_cars.begin();
                
                while(k != i){
                    unpark_cars.push_back(parked_cars.front());
                    parked_cars.pop_front();
                    ++k;
                }
                
                for(deque <char> &dump = unpark_cars; !dump.empty(); dump.pop_front()){
                    parked_cars.push_front(dump.front());
                }
                
                //                parked_cars.erase(i);
                
                break;
            }else{
                std::list<char>::iterator l = parked_cars.end();
                
                if(i == --l){
                    parked_cars.erase(i);
                    break;
                }
                
                while(l != i){
                    unpark_cars.push_back(parked_cars.back());
                    parked_cars.pop_back();
                    l--;
                }
                
                for(deque <char> &dump = unpark_cars; !dump.empty(); dump.pop_front()){
                    parked_cars.push_back(dump.front());
                }
                
                //                parked_cars.erase(i);
                break;
            }
            
        }
        
        j++;
    }
    
    //end loop, and delete item.
    //    if(place_holder != parked_cars.end()){ parked_cars.erase(place_holder); }
    parked_cars.remove(c);
    
}


int main() {
    istream &data = cin;
    read_data(data);
    //do stuff with the data;
    return 0;
}

//ABC
//3
//C
//B
//A
//HIWORLD
//1
//O
//H
//1
//H
//AJKLJKDSL
//0
//FDJKSLAKDKS
//0
//HIWORLD
//1
//D
//STOP

//ABCDEFG
//3
//E
//C
//D
//HIWORLD
//1
//H
//STOP

//ABCD
//1
//B
//STOP


