//
//  main.cpp
//  2019
//
//  Created by Benjamin J. Benson on 11/30/15.
//  Copyright (c) 2015 Benjamin J. Benson. All rights reserved.
//

#include <iostream>
#include <list>
#include <istream>
#include <deque>
using namespace std;

void read_data(std::istream &data);
void move_car(char c);
void create_list(string &io);

list <char> parked_cars;
deque <char> unpark_cars;

void read_data(istream &data){
    
    string original_order;
    int num_moves;
    char c;
    data >> original_order >> num_moves;
    
    create_list(original_order);
    
    int dataset = 1;
    cout << "Dataset #" <<dataset<<": The initial order is "<<original_order<<"."<<endl;
    dataset++;
    
    while(num_moves > 0){
        num_moves--;
        data>>c;
        
        move_car(c);
        
        
        cout << "After "<<c<<" leaves, the order is ";
        
        for(auto i = parked_cars.begin(); i != parked_cars.end(); ++i){
            cout << *i;
        }
        cout << "."<<endl;
//
//        cout <<endl;
        
        if(num_moves == 0){
            data >> original_order;
            parked_cars.clear();
            create_list(original_order);
//            cout << "The original order: "<<original_order<<endl;
            if(original_order == "STOP"){
                break;
            }else{
                data >> num_moves;
                cout << "Dataset #" <<dataset<<": The initial order is "<<original_order<<"."<<endl;
                dataset++;
//                cout << "The number of moves: "<<to_string(num_moves)<<endl;
            }
        }
    }
}

void create_list(string &io){
    for(auto i = io.begin(); i != io.end(); ++i){
        parked_cars.push_back(*i);
    }
}

void move_car(char c){
    int j = 0;
    
    for (auto i = parked_cars.begin(), end = parked_cars.end(); i != end; ++i) {
        
        if(*i == c){
            
            
            if(j <= parked_cars.size()/2){
                auto j = parked_cars.begin();
                
                while(j != i){
                    unpark_cars.push_back(parked_cars.front());
                    parked_cars.pop_front();
                    ++j;
                }
                
                for(auto &dump = unpark_cars; !dump.empty(); dump.pop_front()){
                    parked_cars.push_front(dump.front());
                }
                
                parked_cars.erase(i);
                break;
            }else{
                
                auto j = parked_cars.end();
                
                while(j != i){
                    unpark_cars.push_back(parked_cars.back());
                    parked_cars.pop_back();
                    j--;
                }
                
                for(auto &dump = unpark_cars; !dump.empty(); dump.pop_front()){
                    parked_cars.push_back(dump.front());
                }
                parked_cars.erase(i);
                break;
            }
            
        }
        
        j++;
    }
   
}


int main() {
    istream &data = cin;
    read_data(data);
    //do stuff with the data;
    return 0;
}

//ABCDEFG
//3
//E
//C
//D
//HIWORLD
//1
//H
//H
//1
//H
//STOP