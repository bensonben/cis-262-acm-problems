#include <iostream>
#include <algorithm>
#include <map>
using namespace std;


unsigned long long cloner(unsigned long long& x, unsigned long long& n,unsigned long long& y);
unsigned long long greatest_com_divisor( unsigned long long& a, unsigned long long& b);
unsigned long long calc_fib(unsigned long long & num);
bool is_contained(unsigned long long &num);

//map containing the fibonacci numbers.
map <unsigned long long, unsigned long long> my_map;

void readData (istream& input) {
    int test_cases;
    input >> test_cases;
    int count = 1;
    
    while (test_cases > 0){
        unsigned long long x, n, y, m;
        input >> x >> n >> y >> m;
        cout << "Case " << to_string(count) << ": " << to_string(cloner(x, n, y)) << endl;
        test_cases--;
        count++;
    }
}

unsigned long long calc_fib(unsigned long long & num) {
    if(is_contained(num)){
        return my_map.at(num);
    }
    if (num == 0){
        return 0;
    }
    auto b = num - 1;
    auto c = num - 2;
    
    if (num <= 2){
        return 1;
    }else {
        my_map[num] = calc_fib(b) + calc_fib(c);
        return calc_fib(b) + calc_fib(c);
    }
}

//checks if a key is contained within the map
//returns false if it's not contained in the map
//otherwise returns true.
bool is_contained(unsigned long long &check){
    try{
        my_map.at(check);
    }catch(out_of_range &ex){
        return false;
    }
    return true;
}

unsigned long long cloner(unsigned long long& x, unsigned long long& n, unsigned long long& y) {
    auto temp = n + 1;
    unsigned long long a = (calc_fib(temp) * x) + y;
    unsigned long long b = calc_fib(n) * x;
    return greatest_com_divisor(a, b);
}

unsigned long long greatest_com_divisor ( unsigned long long& x, unsigned long long& y)
{
    unsigned long long z;
    while ( x != 0 ) {
        z = x;
        x = y%x;
        y = z;
    }
    return y;
}

/*Main method. Takes cin data and sends to data processing method*/
int main() {
    istream& data = cin;
    readData(data);
}

/*Test data used for input*/
//6
//4 3 6 5
//5 1 15 2
//944 17 8 53
//4 5 10 5
//1000 70 1000 100000
//1 10 2 10
//1 0 2 10